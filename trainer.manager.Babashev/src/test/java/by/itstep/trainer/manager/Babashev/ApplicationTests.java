package by.itstep.trainer.manager.Babashev;

import by.itstep.trainer.manager.Babashev.entity.*;
import by.itstep.trainer.manager.Babashev.repository.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.List;

@SpringBootTest
class ApplicationTests {

    private AdminEntityRepositoryImpl adminRepository = new AdminEntityRepositoryImpl();
    private AppointmentEntityRepositoryImpl appointmentRepository = new AppointmentEntityRepositoryImpl();
    private CommentEntityRepositoryImpl commentRepository = new CommentEntityRepositoryImpl();
    private TrainerEntityRepositoryImpl trainerRepository = new TrainerEntityRepositoryImpl();

	@BeforeEach
	void setup() {
        System.out.println("Все удалил");
		adminRepository.deleteAll();
		appointmentRepository.deleteAll();
		commentRepository.deleteAll();
		trainerRepository.deleteAll();
	}

    @Test
    void testCreate() {
        System.out.println("начал работу по Create");
        //given
        /**
         * Создаем разные позиции : admin, trainer, comment, appointment
         * После проверяем - создались ли они в базе путем проверки их id.
         */
        AdminEntity admin = EntityFactory.createAdminEntity();

        TrainerEntity trainer = EntityFactory.createTrainerEntity();

        CommentEntity comment = EntityFactory.createCommentEntity();
        comment.setTrainer(trainer);


        AppointmentEntity appointment = EntityFactory.createAppointmentEntity();
        appointment.setTrainer(trainer);

        //when
        /**
         * Отправляем в базу всех по 1, и получаем их ID сохраняем в новый объект.
         */
        System.out.println("Добавляю объекты админов");
        AdminEntity savedAdmin = adminRepository.create(admin);
        TrainerEntity savedTrainer = trainerRepository.create(trainer);
        CommentEntity savedComment = commentRepository.create(comment);
        AppointmentEntity savedAppointment = appointmentRepository.create(appointment);


        //then
        /**
         * Проверяем ID каждого сохраненного объекта, если не null, тест прошел, и все добавились.
         */
        System.out.println("Выполняю проверку админов");
        Assertions.assertNotNull(savedAdmin.getId());
        Assertions.assertNotNull(savedTrainer.getId());
        Assertions.assertNotNull(savedComment.getId());
        Assertions.assertNotNull(savedAppointment.getId());
    }

    @Test
    void testUpdate() {
        //given
        System.out.println("начал работу по Update");
        AdminEntity admin = EntityFactory.createAdminEntity();

        System.out.println("Добавляю админа");
        AdminEntity savedAdmin = adminRepository.create(admin);

        AdminEntity expectedAdminUpdate = AdminEntity.builder()
                .id(savedAdmin.getId())
                .email("admin2@mail.ru")
                .name("Alex-Oi-Oi")
                .build();
        //when
        System.out.println("делаю update");
        AdminEntity actualAdminUpdate = adminRepository.update(expectedAdminUpdate);

        //then
        System.out.println("Проверяю совпадение имен по операции update");
        Assertions.assertEquals(expectedAdminUpdate, actualAdminUpdate.getName());

    }

    @Test
    void testFindAll() {
        //given
        /**
         *  Админы
         */
        System.out.println("Начинаю работу findAll");

        AdminEntity admin = EntityFactory.createAdminEntity();

        AdminEntity adminTwo = AdminEntity.builder()
                .email("aAdmin@mail.ru")
                .name("Tim")
                .build();

        /**
         *  Тренеры
         */
        TrainerEntity trainer = EntityFactory.createTrainerEntity();

        TrainerEntity trainerTwo = TrainerEntity.builder()
                .name("Roma")
                .lastName("Sanuch")
                .workExperience(2L)
                .build();

        /**
         *  Комменты
         */
        CommentEntity comment = EntityFactory.createCommentEntity();
        comment.setTrainer(trainer);

        CommentEntity commentTwo = CommentEntity.builder()
                .content("Позанимался день, все болит. Спасибо.")
                .averageRating(8)
                .published(true)
                .trainer(trainer)
                .build();

        CommentEntity commentThree = CommentEntity.builder()
                .content("Не знаю, как вы выбираете такой персонал...")
                .averageRating(4)
                .published(true)
                .trainer(trainerTwo)
                .build();


        /**
         *  Записи
         */
        AppointmentEntity appointment = EntityFactory.createAppointmentEntity();
        appointment.setTrainer(trainer);

        AppointmentEntity appointmentTwo = AppointmentEntity.builder()
                .name("Dav")
                .lastName("Jordan")
                .email("myEmailJordam@mail.ru")
                .phoneNumber("12312311")
                .message("Привести бы себя в форму")
                .trainer(trainerTwo)
                .build();

        //when
        System.out.println("Добавляю все объекты");
        AdminEntity savedAdmin = adminRepository.create(admin);
        AdminEntity saved2Admin = adminRepository.create(adminTwo);

        TrainerEntity savedTrainer = trainerRepository.create(trainer);
        TrainerEntity saved2Trainer = trainerRepository.create(trainerTwo);

        CommentEntity savedComment = commentRepository.create(comment);
        CommentEntity saved2Comment = commentRepository.create(commentTwo);
        CommentEntity saved3Comment = commentRepository.create(commentThree);

        AppointmentEntity savedAppointment = appointmentRepository.create(appointment);
        AppointmentEntity saved2Appointment = appointmentRepository.create(appointmentTwo);

        //then
        System.out.println("Проверяю все полученные списки на длину");
        Assertions.assertEquals(adminRepository.findAll().size(), 2);
        Assertions.assertEquals(trainerRepository.findAll().size(), 2);
        Assertions.assertEquals(commentRepository.findAll().size(), 3);
        Assertions.assertEquals(appointmentRepository.findAll().size(), 2);
    }

    @Test
    void testDelete() {
        //given
        /**
         * Создаем разные позиции : admin, trainer, comment, appointment
         * После проверяем - создались ли они в базе путем проверки их id.
         */
        System.out.println("Начинаю работу по Delete");
        AdminEntity admin = AdminEntity.builder()
                .email("admin@mail.ru")
                .name("Alex")
                .build();

        TrainerEntity trainer = TrainerEntity.builder()
                .name("Igor")
                .lastName("Stabrovsky")
                .workExperience(12L)
                .build();

        CommentEntity comment = CommentEntity.builder()
                .content("Отличный тренер, всем советую.")
                .averageRating(10)
                .published(true)
                .trainer(trainer)
                .build();


        AppointmentEntity appointment = AppointmentEntity.builder()
                .name("Alex")
                .lastName("Babashev")
                .email("myEmail@gmail.com")
                .phoneNumber("456789")
                .message("Надо подкачаться")
                .trainer(trainer)
                .build();

        System.out.println("Добавляю все объекты");
        AdminEntity savedAdmin = adminRepository.create(admin);
        TrainerEntity savedTrainer = trainerRepository.create(trainer);
        CommentEntity savedComment = commentRepository.create(comment);
        AppointmentEntity savedAppointment = appointmentRepository.create(appointment);
        //when
        /**
         * Отправляем в базу всех по 1, и получаем их ID сохраняем в новый объект.
         * А потом удаляем их методами.
         */

        System.out.println("Удаляю все объекты");
        adminRepository.delete(savedAdmin.getId());
        commentRepository.delete(savedComment.getId());
        appointmentRepository.delete(savedAppointment.getId());
        trainerRepository.delete(savedTrainer.getId());


        //then
        /**
         * Проверяем пуста ли база.
         */
        System.out.println("Проверяю полученные списки объектов из дб.");
        Assertions.assertNull(adminRepository.findById(admin.getId()));
        Assertions.assertNull(trainerRepository.findById(trainer.getId()));
        Assertions.assertNull(commentRepository.findById(comment.getId()));
        Assertions.assertNull(appointmentRepository.findById(appointment.getId()));
    }

    @Test
    void findById() {
	    // GIVEN
        //Создаю каждого объекта по 1 экземпляру.
        AdminEntity admin = EntityFactory.createAdminEntity();

        TrainerEntity trainer = EntityFactory.createTrainerEntity();

        CommentEntity comment = EntityFactory.createCommentEntity();
        comment.setTrainer(trainer);

        AppointmentEntity appointment = EntityFactory.createAppointmentEntity();
        appointment.setTrainer(trainer);

        // Отправляю каждого в базу, получаю ID для каждого, сохраняя их в новый объект.
        AdminEntity savedAdmin = adminRepository.create(admin);
        TrainerEntity savedTrainer = trainerRepository.create(trainer);
        CommentEntity savedComment = commentRepository.create(comment);
        AppointmentEntity savedAppointment = appointmentRepository.create(appointment);

        //Создаю список комментариев у тренера на основе имеющегося комментария к тренеру.
        List<CommentEntity> expectedTrainerComments = List.of(comment);

        // WHEN
        /**
         * Ищу в списке по ID из сохраненного объекта.
         */
        AdminEntity foundAdmin = adminRepository.findById(savedAdmin.getId());

        TrainerEntity foundTrainer = trainerRepository.findById(savedTrainer.getId());

        CommentEntity foundComment = commentRepository.findById(savedComment.getId());

        AppointmentEntity foundAppointment = appointmentRepository.findById(savedAppointment.getId());

        /**
         *  Получаю список комментариев у найденого тренера.
         *  По нему же выставляю FetchType.EAGER т.к. хочу видеть все комментарии при развертывании
         *  Чего нельзя сказать о списке Appointment т.к. не хочу видеть списко людей желающих на запись.
         */
        List<CommentEntity> currentTrainerComments = foundTrainer.getComments();

        // THEN
        /**
         * Делаю различные прверки по ID. и так же проверяю комментарии на соответствие.
         */
        Assertions.assertEquals(foundAdmin.getId(), savedAdmin.getId());
        Assertions.assertEquals(foundComment.getId(), savedComment.getId());
        Assertions.assertEquals(foundAppointment.getId(), savedAppointment.getId());
        Assertions.assertEquals(foundTrainer.getId(), savedTrainer.getId());

        Assertions.assertNotNull(currentTrainerComments);
        Assertions.assertEquals(expectedTrainerComments, currentTrainerComments);
    }

    @Test
    void  findAll() {
	    // GIVEN
        /**
         * Создаю несколько объектов-тренеров. создаю список из этих объектов.
         */
	    TrainerEntity trainer = EntityFactory.createTrainerEntity();
	    TrainerEntity trainerTwo = TrainerEntity.builder().name("David").lastName("Jukovskiy").build();

        TrainerEntity actualTrainerOne = trainerRepository.create(trainer);
        TrainerEntity actualTrainerTwo = trainerRepository.create(trainerTwo);

        List<TrainerEntity> expectedTrainers = List.of(trainer, trainerTwo);
        //  WHEN
        /**
         * Получаю актуальный список тренеров.
         */
        List<TrainerEntity> actualTrainers = trainerRepository.findAll();

        // THEN
        /**
         * Делаю различные проверки.
         */
        Assertions.assertNotNull(actualTrainers);
        Assertions.assertEquals(expectedTrainers.size(), actualTrainers.size());
    }

}
