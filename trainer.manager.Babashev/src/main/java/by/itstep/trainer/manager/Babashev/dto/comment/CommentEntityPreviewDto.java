package by.itstep.trainer.manager.Babashev.dto.comment;

import lombok.Data;

@Data
public class CommentEntityPreviewDto {

    private Long id;
    private String content;
    private int averageRating;

}
