package by.itstep.trainer.manager.Babashev.service.comment;

import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityFullDto;
import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityUpdateDto;
import by.itstep.trainer.manager.Babashev.entity.CommentEntity;
import by.itstep.trainer.manager.Babashev.entity.TrainerEntity;
import by.itstep.trainer.manager.Babashev.exception.InvalidDtoException;
import by.itstep.trainer.manager.Babashev.mapper.CommentEntityMapper;
import by.itstep.trainer.manager.Babashev.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentEntityServiceImpl implements CommentEntityService {

    @Autowired
    private CommentEntityRepository commentRepository;

    @Autowired
    private TrainerEntityRepository trainerRepository;

    @Autowired
    private CommentEntityMapper mapper;

    @Override
    public List<CommentEntityPreviewDto> findAll() {
        List<CommentEntity> comments =  commentRepository.findAll();
        System.out.printf("CommentService -> found comments : %d ", comments.size());
        return mapper.mapToDtoComments(comments);
    }

    @Override
    public CommentEntityFullDto findById(Long id) {
        CommentEntity entity = commentRepository.findById(id);
        System.out.printf("CommentService -> found comment ID: %d", entity.getId());
        return mapper.mapToFullDto(entity);
    }

    @Override
    public CommentEntityFullDto create(CommentEntityCreateDto createDto) throws InvalidDtoException {
//        validateCreateDto(createDto);

        TrainerEntity trainer = trainerRepository.findById(createDto.getTrainerId());
        CommentEntity entity = mapper.mapToEntity(createDto, trainer);

        CommentEntity entityToCreated = commentRepository.create(entity);
        System.out.printf("CommentService -> comment was created id : %s", entityToCreated.getId());
        return mapper.mapToFullDto(entityToCreated);
    }

    @Override
    public CommentEntityFullDto update(CommentEntityUpdateDto updateDto) {
        CommentEntity existingComment = commentRepository.findById(updateDto.getId());

        CommentEntity entity = mapper.mapToEntity(updateDto);
        entity.setPublished(existingComment.isPublished());
        entity.setTrainer(existingComment.getTrainer());

        CommentEntity entityToUpdated = commentRepository.create(entity);
        System.out.printf("CommentService -> comment was updated id : %s", entityToUpdated.getId());
        return mapper.mapToFullDto(entityToUpdated);
    }


    @Override
    public void deleteById(Long id) {
        System.out.printf("CommentService -> comment was deleted id : %s", id);
        commentRepository.delete(id);
    }

    private void validateCreateDto(CommentEntityCreateDto createDto) throws InvalidDtoException {
        if (createDto.getContent() == null) {
            throw new InvalidDtoException();
        }
    }

    @Override
    public List<CommentEntityPreviewDto> findByTrainerId(Long id) {
        List<CommentEntity> comments = commentRepository.findByTrainerId(id);
        return mapper.mapToDtoComments(comments);
    }
}
