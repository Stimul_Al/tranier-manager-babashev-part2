package by.itstep.trainer.manager.Babashev.dto.appointment;

import lombok.Data;

import java.sql.Date;
import java.sql.Timestamp;

@Data
public class AppointmentEntityPreviewDto {

    private Long id;
    private String userName;
    private String phoneNumber;
    private String trainerName;
    private Date startDateOfTraining;
    private String message;

}
