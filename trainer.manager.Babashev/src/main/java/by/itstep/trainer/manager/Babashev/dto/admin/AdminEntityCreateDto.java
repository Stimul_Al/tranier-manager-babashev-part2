package by.itstep.trainer.manager.Babashev.dto.admin;

import lombok.Data;

@Data
public class AdminEntityCreateDto {

    private String name;
    private String lastName;
    private String email;
    private String password;

}
