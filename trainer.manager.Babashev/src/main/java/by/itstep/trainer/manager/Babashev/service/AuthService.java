package by.itstep.trainer.manager.Babashev.service;

import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityLoginDto;
import by.itstep.trainer.manager.Babashev.entity.Role;
import by.itstep.trainer.manager.Babashev.entity.TrainerEntity;

public interface AuthService {

    void login(TrainerEntityLoginDto loginDto);

    void logout();

    boolean isAuthenticated();

    public Role getRole();

    TrainerEntity getLoginedTrainer();
}
