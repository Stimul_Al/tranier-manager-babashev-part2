package by.itstep.trainer.manager.Babashev.exception;

public class InvalidDtoException extends InvalidCreateException{

    public InvalidDtoException() {
        super("One or more fields is null.");
    }

}
