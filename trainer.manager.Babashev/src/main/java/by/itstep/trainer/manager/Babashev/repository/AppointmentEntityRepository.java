package by.itstep.trainer.manager.Babashev.repository;

import by.itstep.trainer.manager.Babashev.entity.AdminEntity;
import by.itstep.trainer.manager.Babashev.entity.AppointmentEntity;

import java.util.List;

public interface AppointmentEntityRepository extends BasedRepository<AppointmentEntity> {
    /**
     * Личный интерфейс для заявки на тренировку, где добавляются методы именно для класса appointment.
     */

    List<AppointmentEntity> findByTrainerId(Long id);
}
