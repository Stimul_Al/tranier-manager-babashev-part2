package by.itstep.trainer.manager.Babashev.repository;

import by.itstep.trainer.manager.Babashev.entity.AdminEntity;
import by.itstep.trainer.manager.Babashev.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class AdminEntityRepositoryImpl
        extends BasedRepositoryImpl<AdminEntity> implements AdminEntityRepository{

    public AdminEntityRepositoryImpl() {
        super(AdminEntity.class, "`admins`;");
    }
}
