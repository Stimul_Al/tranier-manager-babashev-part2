package by.itstep.trainer.manager.Babashev.dto.trainer;

import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityPreviewDto;
import lombok.Data;

import java.util.List;

@Data
public class TrainerEntityFullDto {

    private Long id;
    private String name;
    private String lastName;
    private Long workExperience;
    private String achievements;
    private List<CommentEntityPreviewDto> comments;
    private String imageUrl;
    private String email;
    private String password;
    private List<AppointmentEntityPreviewDto> appointments;

}
