package by.itstep.trainer.manager.Babashev.dto.trainer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrainerEntityPreviewDto {

    private Long id;
    private String name;
    private String lastName;
    private Long workExperience;
    private String achievements;
    private String imageUrl;

}
