package by.itstep.trainer.manager.Babashev.dto.trainer;

import lombok.Data;

@Data
public class TrainerEntityUpdateDto {

    private Long id;
    private String name;
    private String lastName;
    private Long workExperience;
    private String achievements;
    private String imageUrl;
    private String password;

}
