package by.itstep.trainer.manager.Babashev.repository;

import by.itstep.trainer.manager.Babashev.entity.AdminEntity;
import by.itstep.trainer.manager.Babashev.entity.CommentEntity;

import java.util.List;

public interface CommentEntityRepository extends BasedRepository<CommentEntity>{
    /**
     * Личный интерфейс для коммента, где добавляются методы именно для коммента.
     */

    List<CommentEntity> findByTrainerId(Long id);
}
