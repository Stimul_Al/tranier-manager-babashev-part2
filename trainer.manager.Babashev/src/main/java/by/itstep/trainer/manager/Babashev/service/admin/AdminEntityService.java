package by.itstep.trainer.manager.Babashev.service.admin;

import by.itstep.trainer.manager.Babashev.dto.admin.AdminEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.admin.AdminEntityFullDto;
import by.itstep.trainer.manager.Babashev.dto.admin.AdminEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.admin.AdminEntityUpdateDto;
import by.itstep.trainer.manager.Babashev.entity.AdminEntity;
import by.itstep.trainer.manager.Babashev.exception.InvalidCreateException;

import java.util.List;

public interface AdminEntityService {

    List<AdminEntityPreviewDto> findAll();

    AdminEntityFullDto findById(Long id);

    AdminEntityFullDto create(AdminEntityCreateDto createDto) throws InvalidCreateException;

    AdminEntityFullDto update(AdminEntityUpdateDto updateDto);

    void deleteById(Long id);
}
