package by.itstep.trainer.manager.Babashev.repository;

import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.entity.AppointmentEntity;
import by.itstep.trainer.manager.Babashev.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class AppointmentEntityRepositoryImpl
        extends BasedRepositoryImpl<AppointmentEntity> implements AppointmentEntityRepository{

    public AppointmentEntityRepositoryImpl() {
        super(AppointmentEntity.class, "`appointments`;");
    }

    @Override
    public List<AppointmentEntity> findByTrainerId(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<AppointmentEntity> appointments = em.createNativeQuery(
                String.format("SELECT * FROM `appointments` WHERE 'trainer_id' = %d", id), AppointmentEntity.class
        ).getResultList();

        em.close();
        return appointments;
    }
}
