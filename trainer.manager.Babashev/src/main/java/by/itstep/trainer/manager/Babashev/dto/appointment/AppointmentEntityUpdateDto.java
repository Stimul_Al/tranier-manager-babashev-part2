package by.itstep.trainer.manager.Babashev.dto.appointment;

import lombok.Data;

import java.sql.Date;
import java.sql.Timestamp;

@Data
public class AppointmentEntityUpdateDto {

    private Long id;
    private String name;
    private String lastName;
    private String email;
    private String phoneNumber;
    private Long trainerId;
    private Date startDateOfTraining;
    private String message;

}
