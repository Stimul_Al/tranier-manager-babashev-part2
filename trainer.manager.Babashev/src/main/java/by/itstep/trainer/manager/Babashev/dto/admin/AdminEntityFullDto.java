package by.itstep.trainer.manager.Babashev.dto.admin;

import by.itstep.trainer.manager.Babashev.entity.Role;
import lombok.Data;

@Data
public class AdminEntityFullDto {

    private Long id;
    private String userName;
    private String email;
    private String password;
    private Role role;

}
