package by.itstep.trainer.manager.Babashev.service.comment;


import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityFullDto;
import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityUpdateDto;
import by.itstep.trainer.manager.Babashev.entity.CommentEntity;
import by.itstep.trainer.manager.Babashev.exception.InvalidDtoException;

import java.util.List;

public interface CommentEntityService {

    List<CommentEntityPreviewDto> findAll();

    CommentEntityFullDto findById(Long id);

    CommentEntityFullDto create(CommentEntityCreateDto createDto) throws InvalidDtoException;

    CommentEntityFullDto update(CommentEntityUpdateDto updateDto);

    void deleteById(Long id);

    List<CommentEntityPreviewDto> findByTrainerId(Long id);
    
}
