package by.itstep.trainer.manager.Babashev.mapper;

import by.itstep.trainer.manager.Babashev.dto.admin.AdminEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.admin.AdminEntityFullDto;
import by.itstep.trainer.manager.Babashev.dto.admin.AdminEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.admin.AdminEntityUpdateDto;
import by.itstep.trainer.manager.Babashev.entity.AdminEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdminEntityMapper {

    public List<AdminEntityPreviewDto> mapToDtoAdmins(List<AdminEntity> entities) {
        List<AdminEntityPreviewDto> dtoList = new ArrayList();
        for (AdminEntity entity : entities) {
            AdminEntityPreviewDto dto = new AdminEntityPreviewDto();

            dto.setId(entity.getId());
            dto.setEmail(entity.getEmail());
            dto.setUserName(entity.getName() + " " + entity.getLastName());
            dto.setRole(entity.getRole());

            dtoList.add(dto);
        }
        return dtoList;
    }


    public AdminEntityFullDto mapToFullDto(AdminEntity foundAdmin) {
        AdminEntityFullDto dtoPreview = new AdminEntityFullDto();
        dtoPreview.setId(foundAdmin.getId());
        dtoPreview.setUserName(foundAdmin.getName() + " " + foundAdmin.getLastName());
        dtoPreview.setEmail(foundAdmin.getEmail());
        dtoPreview.setRole(foundAdmin.getRole());
        dtoPreview.setPassword(foundAdmin.getPassword());

        return dtoPreview;
    }

    public AdminEntity mapToEntity(AdminEntityCreateDto createDto) {
        AdminEntity admin = new AdminEntity();

        admin.setName(createDto.getName());
        admin.setLastName(createDto.getLastName());
        admin.setEmail(createDto.getEmail());
        admin.setPassword(createDto.getPassword());

        return admin;
    }


    public AdminEntity mapToEntity(AdminEntityUpdateDto updateDto) {
        AdminEntity admin = new AdminEntity();

        admin.setId(updateDto.getId());
        admin.setName(updateDto.getName());
        admin.setLastName(updateDto.getLastName());
        admin.setEmail(updateDto.getEmail());
        admin.setPassword(updateDto.getPassword());

        return admin;
    }


}
