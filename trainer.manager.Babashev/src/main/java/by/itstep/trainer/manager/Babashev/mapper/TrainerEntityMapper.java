package by.itstep.trainer.manager.Babashev.mapper;

import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityFullDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityUpdateDto;
import by.itstep.trainer.manager.Babashev.entity.TrainerEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TrainerEntityMapper {

    private CommentEntityMapper commentMapper = new CommentEntityMapper();

    public List<TrainerEntityPreviewDto> mapToDtoTrainers(List<TrainerEntity> entities) {
        List<TrainerEntityPreviewDto> dtoList = new ArrayList();
        for (TrainerEntity entity : entities) {
            TrainerEntityPreviewDto dto = new TrainerEntityPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setAchievements(entity.getAchievements());
            dto.setWorkExperience(entity.getWorkExperience());
            dto.setImageUrl(entity.getImageUrl());
            dto.setLastName(entity.getLastName());

            dtoList.add(dto);
        }

        return dtoList;
    }


    public TrainerEntityFullDto mapToFullDto(TrainerEntity foundTrainer) {
        TrainerEntityFullDto fullDto = new TrainerEntityFullDto();

        fullDto.setId(foundTrainer.getId());
        fullDto.setName(foundTrainer.getName());
        fullDto.setLastName(foundTrainer.getLastName());
        fullDto.setWorkExperience(foundTrainer.getWorkExperience());
        fullDto.setEmail(foundTrainer.getEmail());
        fullDto.setPassword(foundTrainer.getPassword());
        fullDto.setImageUrl(foundTrainer.getImageUrl());
        fullDto.setAchievements(foundTrainer.getAchievements());

        return fullDto;
    }

    public TrainerEntity mapToEntity(TrainerEntityCreateDto createDto) {
        TrainerEntity trainer = new TrainerEntity();

        trainer.setName(createDto.getName());
        trainer.setLastName(createDto.getLastName());
        trainer.setImageUrl(createDto.getImageUrl());
        trainer.setEmail(createDto.getEmail());
        trainer.setPassword(createDto.getPassword());
        trainer.setAchievements(createDto.getAchievements());
        trainer.setWorkExperience(createDto.getWorkExperience());

        return trainer;
    }


    public TrainerEntity mapToEntity(TrainerEntityUpdateDto updateDto) {
        TrainerEntity trainer = new TrainerEntity();

        trainer.setId(updateDto.getId());
        trainer.setName(updateDto.getName());
        trainer.setLastName(updateDto.getLastName());
        trainer.setWorkExperience(updateDto.getWorkExperience());
        trainer.setAchievements(updateDto.getAchievements());
        trainer.setImageUrl(updateDto.getImageUrl());
        trainer.setPassword(updateDto.getPassword());

        return trainer;
    }
    
    public TrainerEntityPreviewDto mapToPreviewDto(TrainerEntity trainer) {
        TrainerEntityPreviewDto dto = new TrainerEntityPreviewDto();

        dto.setId(trainer.getId());
        dto.setName(trainer.getName());
        dto.setLastName(trainer.getLastName());
        dto.setWorkExperience(trainer.getWorkExperience());
        dto.setAchievements(trainer.getAchievements());
        dto.setImageUrl(trainer.getImageUrl());

        return dto;
    }
    
}
