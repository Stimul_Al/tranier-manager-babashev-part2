package by.itstep.trainer.manager.Babashev.controller;

import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityFullDto;
import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityFullDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityLoginDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.entity.TrainerEntity;
import by.itstep.trainer.manager.Babashev.exception.InvalidCreateException;
import by.itstep.trainer.manager.Babashev.exception.InvalidDtoException;
import by.itstep.trainer.manager.Babashev.repository.TrainerEntityRepository;
import by.itstep.trainer.manager.Babashev.repository.TrainerEntityRepositoryImpl;
import by.itstep.trainer.manager.Babashev.service.AuthService;
import by.itstep.trainer.manager.Babashev.service.AuthServiceImpl;
import by.itstep.trainer.manager.Babashev.service.appointment.AppointmentEntityService;
import by.itstep.trainer.manager.Babashev.service.appointment.AppointmentEntityServiceImpl;
import by.itstep.trainer.manager.Babashev.service.comment.CommentEntityService;
import by.itstep.trainer.manager.Babashev.service.comment.CommentEntityServiceImpl;
import by.itstep.trainer.manager.Babashev.service.trainer.TrainerEntityService;
import by.itstep.trainer.manager.Babashev.service.trainer.TrainerEntityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class TrainerController {

    @Autowired
    private TrainerEntityRepository trainerRep;

    @Autowired
    private TrainerEntityService trainerService;

    @Autowired
    private AppointmentEntityService appointmentService;

    @Autowired
    private CommentEntityService commentService;

    @Autowired
    private AuthService authService;

    @RequestMapping(method = RequestMethod.GET, value = "/index")
    public String openMainPage(Model model) {
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/trainers")
    public String openTrainer(Model model) {
        List<TrainerEntityPreviewDto> found = trainerService.findAll();

        for (TrainerEntityPreviewDto trainer :
             found) {
            System.out.printf("Id : %s, Name : %s, Last name : %s, Image Url : %s, Achievements : %s, Work Experience : %d\n",
                    trainer.getId(), trainer.getName(), trainer.getLastName(), trainer.getImageUrl(), trainer.getAchievements(), trainer.getWorkExperience() );
        }

        model.addAttribute("all_trainers", found);
        return "trainers";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/appointment/{id}")
    public String openAppointmentPage(@PathVariable Long id, Model model) {
        model.addAttribute("createDto",new AppointmentEntityCreateDto());
        model.addAttribute("trainerId", id);
        return "appointment";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/appointment/create")
    public String saveAppointment(AppointmentEntityCreateDto createDto) throws InvalidCreateException {
        appointmentService.create(createDto);
        return "redirect:/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/new-trainer")
    public String openTrainerCreatePage(Model model){
        model.addAttribute("createDto", new TrainerEntityCreateDto());
        return "new-trainer";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/trainers/create")
    public String saveTrainer(TrainerEntityCreateDto createDto) throws InvalidCreateException {
        trainerService.create(createDto);
        return "redirect:/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/trainer-acc/{id}")
    public String getTrainersPage(@PathVariable Long id, Model model) {
        TrainerEntityPreviewDto foundTrainer = trainerService.findById(id);
        System.out.println("Image Url: " + foundTrainer.getImageUrl());
        List<CommentEntityPreviewDto> comments = commentService.findByTrainerId(id);
        List<AppointmentEntityPreviewDto> appointments = appointmentService.findByTrainerId(id);
        model.addAttribute("trainer", foundTrainer);
        model.addAttribute("appointments", appointments);
        model.addAttribute("comments", comments);
        return "trainer-acc";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/sign-in")
    public String getSignInPage(Model model) {
        if(authService.isAuthenticated()) {
            model.addAttribute("userActiveId", authService.getLoginedTrainer().getId());
        }
        model.addAttribute("loginDto", new TrainerEntityLoginDto());
        return "sign-in";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/user/login")
    public String login(TrainerEntityLoginDto loginDto) {
        authService.login(loginDto);
        return "redirect:/trainer-acc/" + authService.getLoginedTrainer().getId();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/comment/{id}")
    public String getCommentPage(@PathVariable Long id, Model model) {
        TrainerEntityPreviewDto found = trainerService.findById(id);
        model.addAttribute("createDto",new CommentEntityCreateDto());
        model.addAttribute("trainer", found);
        return "comment";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/comment/create")
    public String saveComment(CommentEntityCreateDto createDto) throws InvalidDtoException {
        commentService.create(createDto);
        return "redirect:/trainer-acc/" + createDto.getTrainerId();
    }

}
