package by.itstep.trainer.manager.Babashev.service.appointment;

import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityFullDto;
import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityUpdateDto;
import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.entity.AppointmentEntity;
import by.itstep.trainer.manager.Babashev.exception.InvalidCreateException;

import java.util.List;

public interface AppointmentEntityService {

    List<AppointmentEntityPreviewDto> findAll();

    AppointmentEntityFullDto findById(Long id);

    AppointmentEntityFullDto create(AppointmentEntityCreateDto createDto) throws InvalidCreateException;

    AppointmentEntityFullDto update(AppointmentEntityUpdateDto updateDto);

    void deleteById(Long id);

    List<AppointmentEntityPreviewDto> findByTrainerId(Long id);
}
