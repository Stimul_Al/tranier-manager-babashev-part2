package by.itstep.trainer.manager.Babashev.service.appointment;

import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityFullDto;
import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityUpdateDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.entity.AppointmentEntity;
import by.itstep.trainer.manager.Babashev.entity.TrainerEntity;
import by.itstep.trainer.manager.Babashev.exception.InvalidCreateException;
import by.itstep.trainer.manager.Babashev.exception.InvalidDtoException;
import by.itstep.trainer.manager.Babashev.exception.InvalidEmailException;
import by.itstep.trainer.manager.Babashev.mapper.AppointmentEntityMapper;
import by.itstep.trainer.manager.Babashev.mapper.TrainerEntityMapper;
import by.itstep.trainer.manager.Babashev.repository.AppointmentEntityRepository;
import by.itstep.trainer.manager.Babashev.repository.AppointmentEntityRepositoryImpl;
import by.itstep.trainer.manager.Babashev.repository.BasedRepository;
import by.itstep.trainer.manager.Babashev.repository.TrainerEntityRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppointmentEntityServiceImpl implements AppointmentEntityService {

    @Autowired
    private AppointmentEntityRepository appointmentRepository;

    @Autowired
    private AppointmentEntityMapper mapper;

    @Override
    public List<AppointmentEntityPreviewDto> findAll() {
        List<AppointmentEntity> appointments = appointmentRepository.findAll();

        System.out.printf("AppointmentService -> found appointments : %d ", appointments.size());

        return mapper.mapToDtoAppointments(appointments);
    }

    @Override
    public AppointmentEntityFullDto findById(Long id) {
        AppointmentEntity entity = appointmentRepository.findById(id);

        System.out.printf("AppointmentService -> found appointment : %s", entity.getName());

        return mapper.mapToFullDto(entity, getTrainerPreviewDto(entity.getTrainer()));
    }

    @Override
    public AppointmentEntityFullDto create(AppointmentEntityCreateDto createDto) throws InvalidCreateException {
//        validateCreateDto(createDto);

        AppointmentEntity entity = mapper.
                mapToEntity(createDto, getTrainerEntity(createDto.getTrainerId()));

        AppointmentEntity entityToCreated = appointmentRepository.create(entity);
        System.out.printf("AppointmentService -> appointment was created id : %s", entityToCreated.getId());

        return mapper.mapToFullDto(entityToCreated, getTrainerPreviewDto(entityToCreated.getTrainer()));
    }

    @Override
    public AppointmentEntityFullDto update(AppointmentEntityUpdateDto updateDto) {
        AppointmentEntity entity = mapper.mapToEntity(updateDto);

        AppointmentEntity entityToUpdated = appointmentRepository.create(entity);
        System.out.printf("AppointmentService -> appointment was updated id : %s", entityToUpdated.getId());

        return mapper.mapToFullDto(entityToUpdated, getTrainerPreviewDto(entityToUpdated.getTrainer()));
    }

    @Override
    public void deleteById(Long id) {
        System.out.printf("AppointmentService -> appointment was deleted id : %s", id);
        appointmentRepository.delete(id);
    }

    private TrainerEntity getTrainerEntity(Long id) {
        BasedRepository<TrainerEntity> trainerRepository = new TrainerEntityRepositoryImpl();

        TrainerEntity trainer = trainerRepository.findById(id);

        return trainer;
    }

    private TrainerEntityPreviewDto getTrainerPreviewDto(TrainerEntity trainerEntity) {
        TrainerEntityMapper trainerMapper = new TrainerEntityMapper();

        TrainerEntityPreviewDto trainer = trainerMapper.mapToPreviewDto(trainerEntity);

        return trainer;
    }

    @Override
    public List<AppointmentEntityPreviewDto> findByTrainerId(Long id) {
        List<AppointmentEntity> appointments = appointmentRepository.findByTrainerId(id);
        return mapper.mapToDtoAppointments(appointments);
    }

    //    private void validateCreateDto(AppointmentEntityCreateDto createDto) throws InvalidCreateException {
//        if (createDto.getName() == null || createDto.getLastName() == null
//                || createDto.getEmail() == null || createDto.getPhoneNumber() == null) {
//            throw new InvalidDtoException();
//        }
//    }
}
