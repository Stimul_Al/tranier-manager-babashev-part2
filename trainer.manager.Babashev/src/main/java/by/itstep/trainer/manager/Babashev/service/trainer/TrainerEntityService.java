package by.itstep.trainer.manager.Babashev.service.trainer;


import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityFullDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityUpdateDto;
import by.itstep.trainer.manager.Babashev.entity.TrainerEntity;

import java.util.List;

public interface TrainerEntityService {

    List<TrainerEntityPreviewDto> findAll();

    TrainerEntityPreviewDto findById(Long id);

    TrainerEntityFullDto create(TrainerEntityCreateDto createDto);

    TrainerEntityFullDto update(TrainerEntityUpdateDto updateDto);

    void deleteById(Long id);
    
}
