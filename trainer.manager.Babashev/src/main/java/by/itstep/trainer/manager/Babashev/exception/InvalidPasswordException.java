package by.itstep.trainer.manager.Babashev.exception;

public class InvalidPasswordException extends InvalidCreateException{

    public InvalidPasswordException(String message) {
        super(message);
    }

}
