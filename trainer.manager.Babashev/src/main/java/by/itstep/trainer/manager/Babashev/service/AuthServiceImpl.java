package by.itstep.trainer.manager.Babashev.service;

import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityLoginDto;
import by.itstep.trainer.manager.Babashev.entity.Role;
import by.itstep.trainer.manager.Babashev.entity.TrainerEntity;
import by.itstep.trainer.manager.Babashev.repository.TrainerEntityRepository;
import by.itstep.trainer.manager.Babashev.repository.TrainerEntityRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    private TrainerEntity loginedUser;

    @Autowired
    private TrainerEntityRepository trainerRepository;

    @Override
    public void login(TrainerEntityLoginDto loginDto) {
         TrainerEntity foundTrainer = trainerRepository.findByEmail(loginDto.getEmail());
         if (foundTrainer == null) {
            throw new RuntimeException("User not found by email: " + foundTrainer.getEmail());
         }

         if(!foundTrainer.getPassword().equals(loginDto.getPassword())) {
            throw new RuntimeException("User password is incorrect");
         }

        loginedUser = foundTrainer;

    }

    @Override
    public void logout() {
        loginedUser = null;
    }

    @Override
    public boolean isAuthenticated() {
        return loginedUser != null;
    }

    @Override
    public Role getRole() {
        return null;
    }

    @Override
    public TrainerEntity getLoginedTrainer() {
        return loginedUser;
    }
}
