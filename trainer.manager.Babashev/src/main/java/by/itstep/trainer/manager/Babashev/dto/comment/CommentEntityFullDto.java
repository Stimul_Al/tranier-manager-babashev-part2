package by.itstep.trainer.manager.Babashev.dto.comment;

import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityPreviewDto;
import lombok.Data;

@Data
public class CommentEntityFullDto {

    private Long id;
    private String content;
    private int averageRating;
    private boolean published;
    private Long trainerId;

}
