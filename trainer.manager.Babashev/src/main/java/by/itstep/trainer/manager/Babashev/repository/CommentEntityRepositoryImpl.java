package by.itstep.trainer.manager.Babashev.repository;

import by.itstep.trainer.manager.Babashev.entity.CommentEntity;
import by.itstep.trainer.manager.Babashev.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CommentEntityRepositoryImpl
        extends BasedRepositoryImpl<CommentEntity> implements CommentEntityRepository {

    public CommentEntityRepositoryImpl() {
        super(CommentEntity.class, "`comments`;");
    }

    @Override
    public List<CommentEntity> findByTrainerId(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<CommentEntity> comments = em.createNativeQuery(
                String.format("SELECT * FROM `comments` WHERE 'trainer_id' = %d", id),
                CommentEntity.class).getResultList();
        em.close();
        return comments;
    }
}
