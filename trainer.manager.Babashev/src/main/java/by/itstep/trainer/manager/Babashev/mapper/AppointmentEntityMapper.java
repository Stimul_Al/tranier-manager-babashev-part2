package by.itstep.trainer.manager.Babashev.mapper;

import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityFullDto;
import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityUpdateDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.entity.AppointmentEntity;
import by.itstep.trainer.manager.Babashev.entity.TrainerEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AppointmentEntityMapper {

    public List<AppointmentEntityPreviewDto> mapToDtoAppointments(List<AppointmentEntity> entities) {
        List<AppointmentEntityPreviewDto> dtoList = new ArrayList();
        for (AppointmentEntity entity : entities) {
            AppointmentEntityPreviewDto dto = new AppointmentEntityPreviewDto();

            dto.setId(entity.getId());
            dto.setUserName(entity.getName() + " " + entity.getLastName());
            dto.setMessage(entity.getMessage());
            dto.setPhoneNumber(entity.getPhoneNumber());
            dto.setStartDateOfTraining(entity.getStartDateOfTraining());
            dto.setTrainerName(entity.getTrainer().getName());

            dtoList.add(dto);
        }
        return dtoList;
    }


    public AppointmentEntityFullDto mapToFullDto(AppointmentEntity foundAppointment, TrainerEntityPreviewDto trainerDto) {
        AppointmentEntityFullDto dtoPreview = new AppointmentEntityFullDto();
        dtoPreview.setId(foundAppointment.getId());
        dtoPreview.setName(foundAppointment.getName());
        dtoPreview.setLastName(foundAppointment.getLastName());
        dtoPreview.setEmail(foundAppointment.getEmail());
        dtoPreview.setMessage(foundAppointment.getMessage());
        dtoPreview.setPhoneNumber(foundAppointment.getPhoneNumber());
        dtoPreview.setStartDateOfTraining(foundAppointment.getStartDateOfTraining());
        dtoPreview.setTrainer(trainerDto);

        return dtoPreview;
    }

    public AppointmentEntity mapToEntity(AppointmentEntityCreateDto createDto, TrainerEntity trainer) {
        AppointmentEntity appointment = new AppointmentEntity();

        appointment.setName(createDto.getName());
        appointment.setLastName(createDto.getLastName());
        appointment.setEmail(createDto.getEmail());
        appointment.setMessage(createDto.getMessage());
        appointment.setPhoneNumber(createDto.getPhoneNumber());
        appointment.setStartDateOfTraining(createDto.getStartDateOfTraining());
        appointment.setTrainer(trainer);

        return appointment;
    }


    public AppointmentEntity mapToEntity(AppointmentEntityUpdateDto updateDto) {
        AppointmentEntity appointment = new AppointmentEntity();

        appointment.setId(updateDto.getId());
        appointment.setName(updateDto.getName());
        appointment.setLastName(updateDto.getLastName());
        appointment.setEmail(updateDto.getEmail());
        appointment.setMessage(updateDto.getMessage());
        appointment.setPhoneNumber(updateDto.getPhoneNumber());
        appointment.setStartDateOfTraining(updateDto.getStartDateOfTraining());

        return appointment;
    }
    
}
