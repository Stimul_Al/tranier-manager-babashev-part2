package by.itstep.trainer.manager.Babashev.repository;

import by.itstep.trainer.manager.Babashev.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

public class BasedRepositoryImpl<T> implements BasedRepository<T> {
    private static final String SELECT_TEMPLATE = "SELECT * FROM ";
    private static final String DELETE_TEMPLATE = "DELETE FROM ";

    private Class<T> implClass;
    private String table;

    public BasedRepositoryImpl(Class<T> implClass, String table) {
        this.implClass = implClass;
        this.table = table;
    }

    private String getRowsFromTable() {
        return SELECT_TEMPLATE + this.table;
    }

    private String deleteRowsFromTable() {
        return DELETE_TEMPLATE + this.table;
    }

    @Override
    public T findById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        T foundObject = em.find(implClass, id);

        em.close();
        return foundObject;
    }

    @Override
    public List findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List foundListClass = em
                .createNativeQuery(getRowsFromTable(), implClass).getResultList();

        em.close();
        return foundListClass;
    }

    @Override
    public T create(T t) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(t);

        em.getTransaction().commit();
        em.close();
        return t;
    }

    @Override
    public T update(T t) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(t);

        em.getTransaction().commit();
        em.close();
        return t;
    }

    @Override
    public void delete(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Object foundObject = em.find(Object.class, id);
        em.remove(foundObject);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery(deleteRowsFromTable()).executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
