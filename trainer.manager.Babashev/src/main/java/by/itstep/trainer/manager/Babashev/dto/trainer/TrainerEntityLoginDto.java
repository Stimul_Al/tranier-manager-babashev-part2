package by.itstep.trainer.manager.Babashev.dto.trainer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrainerEntityLoginDto {

    private String email;
    private String password;

}
