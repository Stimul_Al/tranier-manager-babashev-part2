package by.itstep.trainer.manager.Babashev.repository;

import by.itstep.trainer.manager.Babashev.entity.AdminEntity;
import by.itstep.trainer.manager.Babashev.entity.TrainerEntity;

import java.util.List;

public interface BasedRepository<T>{
    //Найти по Id
    T findById(Long id);

    //Найти всех
    List<T> findAll();

    //Сохранить новый
    T create(T t);

    //Обновить существущий
    T update(T t);

    //Удалить по Id
    void delete(Long id);

    //Удалить все
    void deleteAll();
}
