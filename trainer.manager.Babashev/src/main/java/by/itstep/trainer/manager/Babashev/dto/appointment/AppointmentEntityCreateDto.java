package by.itstep.trainer.manager.Babashev.dto.appointment;

import by.itstep.trainer.manager.Babashev.entity.TrainerEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentEntityCreateDto {

    private String name;
    private String lastName;
    private String email;
    private String phoneNumber;
    private Long trainerId;
    private Date startDateOfTraining;
    private String message;

    public AppointmentEntityCreateDto(Long trainerId){
        this.trainerId = trainerId;
    }

}
