package by.itstep.trainer.manager.Babashev.service.admin;

import by.itstep.trainer.manager.Babashev.dto.admin.AdminEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.admin.AdminEntityFullDto;
import by.itstep.trainer.manager.Babashev.dto.admin.AdminEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.admin.AdminEntityUpdateDto;
import by.itstep.trainer.manager.Babashev.entity.AdminEntity;
import by.itstep.trainer.manager.Babashev.exception.InvalidCreateException;
import by.itstep.trainer.manager.Babashev.exception.InvalidDtoException;
import by.itstep.trainer.manager.Babashev.exception.InvalidEmailException;
import by.itstep.trainer.manager.Babashev.exception.InvalidPasswordException;
import by.itstep.trainer.manager.Babashev.mapper.AdminEntityMapper;
import by.itstep.trainer.manager.Babashev.repository.AdminEntityRepositoryImpl;
import by.itstep.trainer.manager.Babashev.repository.BasedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminEntityServiceImpl implements AdminEntityService {

    @Autowired
    private BasedRepository<AdminEntity> adminRepository;

    @Autowired
    private AdminEntityMapper mapper;
    private static final char[] FORBIDDEN_SYMBOL = {'/', '<', '>', '\\', '!', '#', '$', '%', '^', '&', '*'};

    @Override
    public List<AdminEntityPreviewDto> findAll() {
        List<AdminEntity> found = adminRepository.findAll();
        System.out.println("AdminService -> found " + found.size() + " admins");
        return mapper.mapToDtoAdmins(found);
    }

    @Override
    public AdminEntityFullDto findById(Long id) {
        AdminEntity foundAdmin = adminRepository.findById(id);
        System.out.printf("AdminService -> found name : %s, last name : %s", foundAdmin.getName(), foundAdmin.getLastName());
        return mapper.mapToFullDto(foundAdmin);
    }

    @Override
    public AdminEntityFullDto create(AdminEntityCreateDto createDto) throws InvalidCreateException {
        validateCreateDto(createDto);

        AdminEntity admin = mapper.mapToEntity(createDto);

        AdminEntity adminToCreate = adminRepository.create(admin);
        System.out.printf("AdminService -> create admin id : %d ", adminToCreate.getId());
        return mapper.mapToFullDto(adminToCreate);
    }

    @Override
    public AdminEntityFullDto update(AdminEntityUpdateDto updateDto) {
        AdminEntity existingAdmin = adminRepository.findById(updateDto.getId());
        AdminEntity admin = mapper.mapToEntity(updateDto);

        AdminEntity adminToUpdate = adminRepository.create(admin);
        adminToUpdate.setRole(existingAdmin.getRole());


        System.out.printf("AdminService -> update admin id : %d ", adminToUpdate.getId());
        return mapper.mapToFullDto(adminToUpdate);
    }

    @Override
    public void deleteById(Long id) {
        adminRepository.delete(id);
    }

    private void validateCreateDto(AdminEntityCreateDto createDto) throws InvalidCreateException {
        if (createDto.getEmail() == null || createDto.getName() == null
                || createDto.getLastName() == null || createDto.getPassword() == null) {
            throw new InvalidDtoException();
        }

        checkEmail(createDto.getEmail());

        checkPassword(createDto.getPassword());

    }

    private void checkEmail(String email) throws InvalidEmailException {
        if (!email.contains("@")) {
            throw new InvalidEmailException();
        }
    }

    private void checkPassword(String password) throws InvalidPasswordException {
        if (password.length() < 8) {
            throw new InvalidPasswordException("Пароль меньше 8 символов.");
        }

        if (!checkingNumbers(password)) {
            throw new InvalidPasswordException("Пароль должен содержать хотя бы одну цифру.");
        }

        if (!checkForForbiddenCharacters(password)) {
            throw new InvalidPasswordException("В пароле не могут использоваться символы : /, <, >, \\, !, #, $, %, ^, &, *");
        }
    }

    private boolean checkingNumbers(String password) {
        boolean check = true;
        int count = 0;
        char[] passwordUser = password.toCharArray();
        for (int i = 0; i < passwordUser.length; i++) {
            if (Character.isDigit(passwordUser[i])) {
                count++;
                break;
            }
            count++;
        }
        if (count == 0) {
            check = false;
        }
        return check;
    }

    private boolean checkForForbiddenCharacters(String password) {
        boolean check = true;
        char[] passwordUser = password.toCharArray();
        for (int i = 0; i < passwordUser.length; i++) {
            for (int j = 0; j < FORBIDDEN_SYMBOL.length; j++) {
                if (passwordUser[i] == FORBIDDEN_SYMBOL[j]) {
                    check = false;
                    break;
                }
            }
        }
        return check;
    }
}
