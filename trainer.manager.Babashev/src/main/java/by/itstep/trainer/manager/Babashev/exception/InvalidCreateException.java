package by.itstep.trainer.manager.Babashev.exception;

public class InvalidCreateException extends Exception {

    public InvalidCreateException(String message) {
        super(message);
    }
}
