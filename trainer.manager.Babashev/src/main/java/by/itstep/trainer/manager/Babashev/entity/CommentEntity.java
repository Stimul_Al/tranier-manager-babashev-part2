package by.itstep.trainer.manager.Babashev.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "`comments`")
public class CommentEntity extends BasedEntity{

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;

    @Column(name = "content")
    private String content;

    @Column(name = "average_rating")
    private int averageRating;

    @Column(name = "published")
    private boolean published; // true -> опубликова; false -> не опубликован (в зависимости от решения админа)

    @ManyToOne
    @JoinColumn(name = "trainer_id")
    private TrainerEntity trainer;
}
