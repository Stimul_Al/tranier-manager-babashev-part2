package by.itstep.trainer.manager.Babashev;

import by.itstep.trainer.manager.Babashev.util.EntityManagerUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManager;

@SpringBootApplication
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

		EntityManager em = EntityManagerUtils.getEntityManager();
	}

}
