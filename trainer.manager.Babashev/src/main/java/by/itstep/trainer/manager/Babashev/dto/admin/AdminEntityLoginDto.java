package by.itstep.trainer.manager.Babashev.dto.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminEntityLoginDto {

    private String email;
    private String password;

}
