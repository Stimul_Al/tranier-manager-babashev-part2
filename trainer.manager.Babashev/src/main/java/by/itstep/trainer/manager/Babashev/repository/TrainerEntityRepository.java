package by.itstep.trainer.manager.Babashev.repository;

import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityLoginDto;
import by.itstep.trainer.manager.Babashev.entity.AdminEntity;
import by.itstep.trainer.manager.Babashev.entity.TrainerEntity;

import java.util.List;

public interface TrainerEntityRepository extends BasedRepository<TrainerEntity>{
    /**
     * Личный интерфейс для тренера, где добавляются методы именно для тренера.
     */

    TrainerEntity findByEmail(String email);
}
