package by.itstep.trainer.manager.Babashev.dto.comment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class CommentEntityCreateDto {

    private String content;
    private int averageRating;
    private Long trainerId;
}
