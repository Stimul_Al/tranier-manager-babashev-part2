package by.itstep.trainer.manager.Babashev.mapper;

import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityFullDto;
import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityUpdateDto;
import by.itstep.trainer.manager.Babashev.entity.CommentEntity;
import by.itstep.trainer.manager.Babashev.entity.TrainerEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentEntityMapper {

    public List<CommentEntityPreviewDto> mapToDtoComments(List<CommentEntity> entities) {
        List<CommentEntityPreviewDto> dtoPreviewList = new ArrayList();

        for (CommentEntity entity : entities) {
            CommentEntityPreviewDto dto = new CommentEntityPreviewDto();

            dto.setId(entity.getId());
            dto.setContent(entity.getContent());
            dto.setAverageRating(entity.getAverageRating());

            dtoPreviewList.add(dto);
        }
        return dtoPreviewList;
    }


    public CommentEntityFullDto mapToFullDto(CommentEntity foundComment) {
        CommentEntityFullDto dtoPreview = new CommentEntityFullDto();

        dtoPreview.setId(foundComment.getId());
        dtoPreview.setAverageRating(foundComment.getAverageRating());
        dtoPreview.setContent(foundComment.getContent());
        dtoPreview.setPublished(foundComment.isPublished());
        dtoPreview.setTrainerId(foundComment.getTrainer().getId());

        return dtoPreview;
    }

    public CommentEntity mapToEntity(CommentEntityCreateDto createDto, TrainerEntity trainer) {
        CommentEntity comment = new CommentEntity();

        comment.setContent(createDto.getContent());
        comment.setAverageRating(createDto.getAverageRating());
        comment.setTrainer(trainer);

        return comment;
    }


    public CommentEntity mapToEntity(CommentEntityUpdateDto updateDto) {
        CommentEntity comment = new CommentEntity();

        comment.setId(updateDto.getId());
        comment.setContent(updateDto.getContent());
        comment.setAverageRating(updateDto.getAverageRating());

        return comment;
    }
    
}
