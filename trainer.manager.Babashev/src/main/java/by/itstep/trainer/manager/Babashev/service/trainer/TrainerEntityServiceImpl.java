package by.itstep.trainer.manager.Babashev.service.trainer;

import by.itstep.trainer.manager.Babashev.dto.appointment.AppointmentEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.comment.CommentEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityCreateDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityFullDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityPreviewDto;
import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityUpdateDto;
import by.itstep.trainer.manager.Babashev.entity.AppointmentEntity;
import by.itstep.trainer.manager.Babashev.entity.CommentEntity;
import by.itstep.trainer.manager.Babashev.entity.TrainerEntity;
import by.itstep.trainer.manager.Babashev.exception.InvalidCreateException;
import by.itstep.trainer.manager.Babashev.exception.InvalidDtoException;
import by.itstep.trainer.manager.Babashev.exception.InvalidEmailException;
import by.itstep.trainer.manager.Babashev.exception.InvalidPasswordException;
import by.itstep.trainer.manager.Babashev.mapper.AppointmentEntityMapper;
import by.itstep.trainer.manager.Babashev.mapper.CommentEntityMapper;
import by.itstep.trainer.manager.Babashev.mapper.TrainerEntityMapper;
import by.itstep.trainer.manager.Babashev.repository.TrainerEntityRepositoryImpl;
import by.itstep.trainer.manager.Babashev.repository.BasedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrainerEntityServiceImpl implements TrainerEntityService {

    @Autowired
    private BasedRepository<TrainerEntity> trainerRepository;

    @Autowired
    private TrainerEntityMapper mapper;

    private static final char[] FORBIDDEN_SYMBOL = {'/', '<', '>', '\\', '!', '#', '$', '%', '^', '&', '*'};

    @Override
    public List<TrainerEntityPreviewDto> findAll() {
        List<TrainerEntity> trainers = trainerRepository.findAll();
        System.out.printf("TrainerService -> found trainers : %d ", trainers.size());
        return mapper.mapToDtoTrainers(trainers);
    }

    @Override
    public TrainerEntityPreviewDto findById(Long id) {
        TrainerEntity entity = trainerRepository.findById(id);
        System.out.printf("TrainerService -> found trainer : %s", entity.getName());
        return mapper.mapToPreviewDto(entity);
    }


    @Override
    public TrainerEntityFullDto create(TrainerEntityCreateDto createDto) {
        TrainerEntity entity = mapper.mapToEntity(createDto);

        TrainerEntity entityToCreated = trainerRepository.create(entity);
        System.out.printf("TrainerService -> trainer was created id : %s", entityToCreated.getId());
        return mapper.mapToFullDto(entityToCreated);
    }

    @Override
    public TrainerEntityFullDto update(TrainerEntityUpdateDto updateDto) {
        TrainerEntity entity = mapper.mapToEntity(updateDto);
        TrainerEntity precist = trainerRepository.findById(updateDto.getId());
        entity.setComments(precist.getComments());
        entity.setAppointments(precist.getAppointments());

        TrainerEntity entityToUpdated = trainerRepository.update(entity);
        System.out.printf("TrainerService -> trainer was updated id : %s", entityToUpdated.getId());
        return mapper.mapToFullDto(entityToUpdated);
    }


    @Override
    public void deleteById(Long id) {
        System.out.printf("TrainerService -> trainer was deleted id : %s", id);
        trainerRepository.delete(id);
    }

    private List<CommentEntityPreviewDto> getPreviewComments(List<CommentEntity> commentEntities) {
        CommentEntityMapper commentMapper = new CommentEntityMapper();
        return commentMapper.mapToDtoComments(commentEntities);
    }

    private List<AppointmentEntityPreviewDto> getPreviewAppointment(List<AppointmentEntity> appointmentEntities) {
        AppointmentEntityMapper appointmentMapper = new AppointmentEntityMapper();
        return appointmentMapper.mapToDtoAppointments(appointmentEntities);
    }

    private void validate(TrainerEntityCreateDto createDto) throws InvalidCreateException {
        if (createDto.getName() == null || createDto.getLastName() == null ||
                createDto.getEmail() == null || createDto.getPassword() == null) {
            throw new InvalidDtoException();
        }

        checkEmail(createDto.getEmail());

        checkPassword(createDto.getPassword());
    }

    private void checkEmail(String email) throws InvalidEmailException {
        if (!email.contains("@")) {
            throw new InvalidEmailException();
        }
    }

    private void checkPassword(String password) throws InvalidPasswordException {
        if (password.length() < 8) {
            throw new InvalidPasswordException("Пароль меньше 8 символов.");
        }

        if (!checkingNumbers(password)) {
            throw new InvalidPasswordException("Пароль должен содержать хотя бы одну цифру.");
        }

        if(!checkForForbiddenCharacters(password)) {
            throw new InvalidPasswordException("В пароле не могут использоваться символы : /, <, >, \\, !, #, $, %, ^, &, *");
        }

    }

    private boolean checkingNumbers(String password) {
        boolean check = true;
        int count = 0;
        char[] passwordUser = password.toCharArray();
        for (int i = 0; i < passwordUser.length; i++) {
            if (Character.isDigit(passwordUser[i])) {
                count++;
                break;
            }
            count++;
        }
        if (count == 0) {
            check = false;
        }
        return check;
    }

    private boolean checkForForbiddenCharacters(String password) {
        boolean check = true;
        char[] passwordUser = password.toCharArray();
        for (int i = 0; i < passwordUser.length; i++) {
            for (int j = 0; j < FORBIDDEN_SYMBOL.length; j++) {
                if (passwordUser[i] == FORBIDDEN_SYMBOL[j]) {
                    check = false;
                    break;
                }
            }
        }
        return check;
    }


}
