package by.itstep.trainer.manager.Babashev.repository;

import by.itstep.trainer.manager.Babashev.entity.AdminEntity;
import com.fasterxml.jackson.databind.ser.Serializers;

import java.util.List;

public interface AdminEntityRepository extends BasedRepository<AdminEntity> {

    /**
     * Личный интерфейс для админа, где добавляются методы именно для админа.
     */
}
