package by.itstep.trainer.manager.Babashev.repository;

import by.itstep.trainer.manager.Babashev.dto.trainer.TrainerEntityLoginDto;
import by.itstep.trainer.manager.Babashev.entity.TrainerEntity;
import by.itstep.trainer.manager.Babashev.util.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class TrainerEntityRepositoryImpl
        extends BasedRepositoryImpl<TrainerEntity> implements TrainerEntityRepository {

    public TrainerEntityRepositoryImpl() {
        super(TrainerEntity.class, "`trainers`;");
    }


    @Override
    public TrainerEntity findById(Long id) {
        TrainerEntity foundTrainer = super.findById(id);

        init(foundTrainer);

        return foundTrainer;
    }

    @Override
    public TrainerEntity findByEmail(String email) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        System.out.println("Email by trainer = " + email);
        TrainerEntity foundTrainer = (TrainerEntity) em.createNativeQuery(String.format(
                "SELECT * FROM `trainers` WHERE `email`  = \"%s\"", email),
                TrainerEntity.class).getSingleResult();
        System.out.printf("Found entity : \nid: %d, email: %s, password: %s",
                foundTrainer.getId(), foundTrainer.getEmail(), foundTrainer.getPassword());
        em.close();
        return foundTrainer;
    }

    private void init (TrainerEntity foundTrainer) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        TrainerEntity mergeTrainer = em.merge(foundTrainer);

        Hibernate.initialize(mergeTrainer.getComments());

        em.close();
    }
}
