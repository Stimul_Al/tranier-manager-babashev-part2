package by.itstep.trainer.manager.Babashev.exception;

public class InvalidEmailException extends InvalidCreateException{

    public InvalidEmailException() {
        super("Mail is incorrect");
    }

}
