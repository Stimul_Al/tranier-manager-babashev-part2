package by.itstep.trainer.manager.Babashev.entity;

import java.sql.Timestamp;

public class EntityFactory {

    public static AdminEntity createAdminEntity() {
        return AdminEntity.builder()
                .email("admin@mail.ru")
                .name("Alex")
                .build();
    }

    public static TrainerEntity createTrainerEntity() {
        return TrainerEntity.builder()
                .name("Igor")
                .lastName("Stabrovsky")
                .workExperience(12L)
                .build();
    }

    public static AppointmentEntity createAppointmentEntity() {
        return AppointmentEntity.builder()
                .name("Alex")
                .lastName("Babashev")
                .email("myEmail@gmail.com")
                .phoneNumber("456789")
                .message("Надо подкачаться")
                .build();
    }

    public static CommentEntity createCommentEntity() {
        return CommentEntity.builder()
                .content("Отличный тренер, всем советую.")
                .averageRating(10)
                .published(true)
                .build();
    }
}
